<?php
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
  }
  $errors = array();
  $errors['name'] = !empty($_COOKIE['name_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['superpowers'] = !empty($_COOKIE['superpowers_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  $errors['know_check'] = !empty($_COOKIE['know_check_error']);
  $errors['date1'] = !empty($_COOKIE['date1_error']);
  $errors['member_num'] = !empty($_COOKIE['member_num_error']);
  $errors['gender'] = !empty($_COOKIE['gender_error']);

  if ($errors['name']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('name_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните имя.</div>';
  }
  if ($errors['email']) {
    setcookie('email_error', '', 100000);
    $messages[] = '<div class="error">Заполните поле email.</div>';
  }
  if ($errors['superpowers']) {
    setcookie('superpowers_error', '', 100000);
    $messages[] = '<div class="error">Выберите хотя бы одну сверхспособность.</div>';
  }
  if ($errors['biography']) {
    setcookie('biography_error', '', 100000);
    $messages[] = '<div class="error">Заполните поле биография.</div>';
  }
  if ($errors['know_check']) {
    setcookie('know_check_error', '', 100000);
    $messages[] = '<div class="error">Потвердите ознакомление с контрактом.</div>';
  }
  if ($errors['date1']) {
    setcookie('date1_error', '', 100000);
    $messages[] = '<div class="error">Выберите дату рождения.</div>';
  }
  if ($errors['member_num']) {
    setcookie('member_num_error', '', 100000);
    $messages[] = '<div class="error">Выберите число конечностей.</div>';
  }
  if ($errors['gender']) {
    setcookie('gender_error', '', 100000);
    $messages[] = '<div class="error">Выберите пол.</div>';
  }

   // Складываем предыдущие значения полей в массив, если есть.
   $values = array();
   $values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
   $values['superpowers'] = empty($_COOKIE['superpowers_value']) ? '' : $_COOKIE['superpowers_value'];
   $values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
   $values['know_check'] = empty($_COOKIE['know_check_value']) ? '' : $_COOKIE['know_check_value'];
   $values['date1'] = empty($_COOKIE['date1_value']) ? '' : $_COOKIE['date1_value'];
   $values['member'] = empty($_COOKIE['member_value']) ? '' : $_COOKIE['member_value'];
   $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
   $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
   $values['member_num'] = empty($_COOKIE['member_num_value']) ? '' : $_COOKIE['member_num_value'];
  // Включаем содержимое файла form.php.
  include('form.php');
  // Завершаем работу скрипта.
  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.
$errors = FALSE;
if (empty($_POST['name'])) {
  setcookie('name_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
} else {
  // Сохраняем ранее введенное в форму значение на месяц.
  setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['email'])){
  setcookie('email_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
} else {
  // Сохраняем ранее введенное в форму значение на месяц.
  setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['superpowers'])){
  setcookie('superpowers_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
} else {
  setcookie('superpowers_value', $_POST['superpowers'], time() + 30 * 24 * 60 * 60);
  $powers = serialize($_POST['superpowers']);
}
if (empty($_POST['biography'])){
  setcookie('biography_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}  else {
  
  setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['know_check'])){
  setcookie('know_check_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}else {
  setcookie('know_check_value', $_POST['know_check'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['date1'])){
  setcookie('date1_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}else {
  setcookie('date1_value', $_POST['date1'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['member_num'])){
  setcookie('member_num_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}else {
  setcookie('member_num_value', $_POST['member_num'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['gender'])){
  setcookie('gender_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}else {
  setcookie('gender_value', $_POST['gender'], time() + 30 * 24 * 60 * 60);
}
if ($errors) {
  // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
  header('Location: index.php');
  exit();
}else {
  // Удаляем Cookies с признаками ошибок.
  setcookie('name_error', '', 100000);
  setcookie('email_error', '', 100000);
  setcookie('biography_error', '', 100000);
  setcookie('know_check_error', '', 100000);
  setcookie('date1_error', '', 100000);
  setcookie('member_num_error', '', 100000);
  setcookie('gender_error', '', 100000);
}
// Сохранение в базу данных.

$user = 'u20495'; $pass = '1204499';
    
$db = new PDO('mysql:host=localhost;dbname=u20495', $user, $pass, array(
PDO::ATTR_PERSISTENT => true
));
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

try {
    $stmt = $db->prepare("INSERT INTO table1 SET name = ?, email = ?, date = ?, sex = ?, limbs = ?, powers = ?, bio = ?");
    $stmt->execute(array(
        $_POST['name'],
        $_POST['email'],
        $_POST['date1'],
        $_POST['gender'],
        $_POST['member_num'],
        $powers,
        $_POST['biography'],
    ));
}
catch(PDOException $e) {
    print ('Ошибка: ' . $e->getMessage());
    exit();
}
// Сохраняем куку с признаком успешного сохранения.
setcookie('save', '1');
// Делаем перенаправление.
header('Location: index.php');
//  stmt - это "дескриптор состояния".
 
//  Именованные метки.
//$stmt = $db->prepare("INSERT INTO test (label,color) VALUES (:label,:color)");
//$stmt -> execute(array('label'=>'perfect', 'color'=>'green'));
 
//Еще вариант
/*$stmt = $db->prepare("INSERT INTO users (firstname, lastname, email) VALUES (:firstname, :lastname, :email)");
$stmt->bindParam(':firstname', $firstname);
$stmt->bindParam(':lastname', $lastname);
$stmt->bindParam(':email', $email);
$firstname = "John";
$lastname = "Smith";
$email = "john@test.com";
$stmt->execute();
*/

