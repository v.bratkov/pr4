<!DOCTYPE html>
<html lang="ru">

<head>
  <title>Web3/index</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="./style.css">
  <meta name="viewport" content="width=device-width">
</head>

<body>
  <div class="content">
  <?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>
    <form id="form" action="" method="POST">
      <h3>Форма</h3>
      <label>Имя:
        <input type="text" <?php if ($errors['name']) {print 'class="error"';} ?> value="<?php print $values['name']; ?>"  name='name'><br>
      </label>
      <label>E-mail:
        <input type="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" name="email"><br>
      </label>
      <label>Дата рождения:
        <input type="date" <?php if ($errors['date1']) {print 'class="error"';} ?> id="date1" value="<?php print $values['date1']; ?>" name="date1"><br>
      </label>
      <label>Пол
        <input type="radio" <?php if ($errors['gender']) {print 'class="error"';} ?> value="M" <?php if ($values['gender']=='M') {print 'checked="checked"';}?> name="gender">Мужской
      </label>
      <label>
        <input type="radio" <?php if ($errors['gender']) {print 'class="error"';} ?> value="W"<?php if ($values['gender']=='') {print 'checked="checked"';}else{if ($values['gender']=='W') {print 'checked="checked"';}} ?>  name="gender">Женский<br>
      </label>
      <label>Количество конечностей
        <input type="radio" <?php if ($errors['member_num']) {print 'class="error"';} ?> value="1" <?php if ($values['member_num']=='') {print 'checked="checked"';}else{if ($values['member_num']==1) {print 'checked="checked"';}}?> name="member_num">1
      </label>
      <label>
        <input type="radio" <?php if ($errors['member_num']) {print 'class="error"';} ?> value="2"<?php if ($values['member_num']==2) {print 'checked="checked"';}?> name="member_num">2
      </label>
      <label>
        <input type="radio" <?php if ($errors['member_num']) {print 'class="error"';} ?> value="3"<?php if ($values['member_num']==3) {print 'checked="checked"';}?> name="member_num"> 3
      </label>
      <label>
        <input type="radio" <?php if ($errors['member_num']) {print 'class="error"';} ?> value="4" <?php if ($values['member_num']==4) {print 'checked="checked"';}?> name="member_num"> 4<br>
      </label>
      <label for="superpowers">Сверхспособности<br>
        <select name="superpowers" <?php if ($errors['superpowers']) {print 'class="error"';} ?> multiple="multiple" id="superpowers">
          <option value="immortal"> Бессмертие </option>
          <option value="throughWalls">Прохождение сквозь стены</option>
          <option value="levitation"> Левитация </option>
        </select><br>
      </label>
      <label>Биография<br>
        <textarea name="biography" id="biography" cols="25" rows="7" <?php if ($errors['biography']) {print 'class="error"';} ?>><?php print $values['biography']; ?></textarea><br>
      </label>
      <label>
        <input type="checkbox" name="know_check" ><span <?php if ($errors['know_check']) {print 'class="error"';} ?>>С контрактом ознакомлен(а)</span>
      </label><br>
      <button name="submitted">Отправить</button>
    </form>
  </div>
</body>

</html>